<?php
namespace ext\javafx;

/**
 * Class ProgressIndicator
 * @package ext\javafx
 */
class UXProgressIndicator extends UXControl
{
    /**
     * @var double
     */
    public $progress;

    /**
     * @readonly
     * @var bool
     */
    public $indeterminate;
}