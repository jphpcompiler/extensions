<?php
namespace ext\javafx;

/**
 * Class UXImageView
 * @package ext\javafx
 */
class UXImageView extends UXNode
{
    /**
     * @var UXImage
     */
    public $image;

    /**
     * @var bool
     */
    public $smooth;

    /**
     * @var bool
     */
    public $preserveRatio;
}