<?php
namespace ext\javafx;

/**
 * Class UXMenuBar
 * @package ext\javafx
 */
class UXMenuBar extends UXControl
{
    /**
     * @var UXList of UXMenu
     */
    public $menus;
}