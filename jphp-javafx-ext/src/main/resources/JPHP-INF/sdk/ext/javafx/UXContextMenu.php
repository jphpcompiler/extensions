<?php
namespace ext\javafx;

/**
 * Class UXContextMenu
 * @package ext\javafx
 */
class UXContextMenu extends UXPopupWindow
{
    /**
     * @var UXList
     */
    public $items;
}