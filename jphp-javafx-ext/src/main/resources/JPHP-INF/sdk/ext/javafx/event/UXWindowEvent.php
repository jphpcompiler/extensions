<?php
namespace ext\javafx\event;

use ext\javafx\UXWindow;

/**
 * Class UXWindowEvent
 * @package ext\javafx\event
 */
class UXWindowEvent {
    /**
     * @var UXWindow
     */
    public $target;
}