<?php
namespace ext\javafx;

/**
 * Class UXMenu
 * @package ext\javafx
 */
class UXMenu extends UXMenuItem
{
    /**
     * @readonly
     * @var UXList of UXMenuItem
     */
    public $items;

    /**
     * @readonly
     * @var bool
     */
    public $showing;

    /**
     * ...
     */
    public function show() {}

    /**
     * ...
     */
    public function hide() {}
}