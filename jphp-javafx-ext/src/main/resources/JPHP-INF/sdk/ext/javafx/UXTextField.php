<?php
namespace ext\javafx;

/**
 * Class UXTextField
 * @package ext\javafx
 */
class UXTextField extends UXTextInputControl
{
    /**
     * @var string
     */
    public $alignment;

    /**
     * @var int
     */
    public $prefColumnCount;
}