<?php
namespace ext\javafx\layout;

use ext\javafx\UXList;
use ext\javafx\UXNode;
use ext\javafx\UXParent;

/**
 * Class UXPane
 * @package ext\javafx
 */
class UXPane extends UXParent
{
    /**
     * @readonly
     * @var UXList
     */
    public $children;
}