<?php
namespace ext\javafx;

/**
 * Class UXWebView
 * @package ext\javafx
 */
class UXWebView extends UXParent
{
    /**
     * @readonly
     * @var UXWebEngine
     */
    public $engine;

    /**
     * @var bool
     */
    public $contextMenuEnabled;
}