<?php
namespace ext\jdbc;

/**
 * Class Driver
 */
class Driver {
    private function __construct() { }

    /**
     * Retrieves whether the driver thinks that it can open a connection
     * to the given URL.  Typically drivers will return true if they
     * understand the subprotocol specified in the URL and false if
     * they do not.
     *
     * @param string $url
     * @return bool
     * @throws SQLException if a database access error occurs
     */
    public function acceptsURL($url) { }

    /**
     * Retrieves the driver's major version number. Initially this should be 1.
     * @return int
     */
    public function getMajorVersion() { }

    /**
     * Gets the driver's minor version number. Initially this should be 0.
     * @return int
     */
    public function getMinorVersion() { }

    /**
     * @return bool
     */
    public function jdbcCompliant() { }
}