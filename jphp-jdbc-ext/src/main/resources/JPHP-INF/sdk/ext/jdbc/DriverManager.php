<?php
namespace ext\jdbc;

/**
 * Class DriverManager
 */
final class DriverManager {
    private function __construct() { }

    /**
     * Registers a jdbc driver
     * @param string $driverClass for example 'com.mysql.jdbc.Driver'
     */
    public static function register($driverClass) { }

    /**
     * Attempts to establish a connection to the given database URL.
     * The DriverManager attempts to select an appropriate driver from
     * the set of registered JDBC drivers.
     *
     * @param string $url
     * @param string $user (optional)
     * @param string $password (optional)
     * @throws SQLException if a database access error occurs
     *
     * @return Connection
     */
    public static function getConnection($url, $user, $password) { }

    /**
     * @param string $url
     * @return Driver
     */
    public static function getDriver($url) { }
}