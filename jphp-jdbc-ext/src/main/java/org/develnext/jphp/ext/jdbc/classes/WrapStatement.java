package org.develnext.jphp.ext.jdbc.classes;

import org.develnext.jphp.ext.jdbc.JdbcExtension;
import php.runtime.annotation.Reflection;
import php.runtime.annotation.Reflection.Name;
import php.runtime.annotation.Reflection.Signature;
import php.runtime.annotation.Reflection.WrapInterface;
import php.runtime.env.Environment;
import php.runtime.lang.BaseWrapper;
import php.runtime.reflection.ClassEntity;

import java.sql.Statement;

@Name(JdbcExtension.NAMESPACE + "Statement")
@WrapInterface(value = Statement.class, skipConflicts = true)
public class WrapStatement extends BaseWrapper<Statement> {
    public WrapStatement(Environment env, Statement wrappedObject) {
        super(env, wrappedObject);
    }

    public WrapStatement(Environment env, ClassEntity clazz) {
        super(env, clazz);
    }

    @Signature
    private void __construct() { }
}
