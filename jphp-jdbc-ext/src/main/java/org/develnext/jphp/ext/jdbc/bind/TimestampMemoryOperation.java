package org.develnext.jphp.ext.jdbc.bind;

import php.runtime.Memory;
import php.runtime.env.Environment;
import php.runtime.env.TraceInfo;
import php.runtime.ext.core.classes.time.WrapTime;
import php.runtime.memory.ObjectMemory;
import php.runtime.memory.support.MemoryOperation;
import php.runtime.reflection.ParameterEntity;

import java.sql.Time;
import java.sql.Timestamp;

public class TimestampMemoryOperation extends MemoryOperation<Timestamp> {
    @Override
    public Class<?>[] getOperationClasses() {
        return new Class<?>[] { Timestamp.class };
    }

    @Override
    public Timestamp convert(Environment environment, TraceInfo traceInfo, Memory memory) {
        return memory.isNull() ? null : new Timestamp(memory.toObject(WrapTime.class).getDate().getTime());
    }

    @Override
    public Memory unconvert(Environment environment, TraceInfo traceInfo, Timestamp date) {
        return new ObjectMemory(new WrapTime(environment, new java.util.Date(date.getTime())));
    }

    @Override
    public void applyTypeHinting(ParameterEntity parameter) {
        parameter.setTypeNativeClass(WrapTime.class);
    }
}
