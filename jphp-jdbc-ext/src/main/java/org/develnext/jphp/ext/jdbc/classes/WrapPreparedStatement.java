package org.develnext.jphp.ext.jdbc.classes;

import org.develnext.jphp.ext.jdbc.JdbcExtension;
import php.runtime.annotation.Reflection;
import php.runtime.annotation.Reflection.Name;
import php.runtime.annotation.Reflection.Signature;
import php.runtime.annotation.Reflection.WrapInterface;
import php.runtime.env.Environment;
import php.runtime.reflection.ClassEntity;

import java.math.BigDecimal;
import java.sql.*;

@Name(JdbcExtension.NAMESPACE + "PreparedStatement")
@WrapInterface(WrapPreparedStatement.Methods.class)
public class WrapPreparedStatement extends WrapStatement {
    public WrapPreparedStatement(Environment env, PreparedStatement wrappedObject) {
        super(env, wrappedObject);
    }

    public WrapPreparedStatement(Environment env, ClassEntity clazz) {
        super(env, clazz);
    }

    interface Methods {
        void setNull(int parameterIndex, int sqlType);
        void setBoolean(int parameterIndex, boolean x);
        void setByte(int parameterIndex, byte x);
        void setShort(int parameterIndex, short x);
        void setInt(int parameterIndex, int x);
        void setLong(int parameterIndex, long x);
        void setFloat(int parameterIndex, float x);
        void setDouble(int parameterIndex, double x);
        void setBigDecimal(int parameterIndex, BigDecimal x);
        void setString(int parameterIndex, String x);
        void setBytes(int parameterIndex, byte x[]);

        void setDate(int parameterIndex, java.sql.Date x);
        void setTime(int parameterIndex, java.sql.Time x);
        void setTimestamp(int parameterIndex, java.sql.Timestamp x);
        void setBlob (int parameterIndex, Blob x);
        void setClob (int parameterIndex, Clob x);
        void setURL(int parameterIndex, java.net.URL x);
        void setNString(int parameterIndex, String value);
        void setNClob(int parameterIndex, NClob value);

        void clearParameters();
        boolean execute();
        void addBatch();
    }
}
