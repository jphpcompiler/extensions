package org.develnext.jphp.ext.jdbc.classes;

import org.develnext.jphp.ext.jdbc.JdbcExtension;
import php.runtime.annotation.Reflection;
import php.runtime.annotation.Reflection.Name;
import php.runtime.annotation.Reflection.Signature;
import php.runtime.annotation.Reflection.WrapInterface;
import php.runtime.env.Environment;
import php.runtime.lang.BaseWrapper;
import php.runtime.reflection.ClassEntity;

import java.sql.Connection;

@Name(JdbcExtension.NAMESPACE)
@WrapInterface(value = Connection.class, skipConflicts = true)
public class WrapConnection extends BaseWrapper<Connection> {
    public WrapConnection(Environment env, Connection wrappedObject) {
        super(env, wrappedObject);
    }

    public WrapConnection(Environment env, ClassEntity clazz) {
        super(env, clazz);
    }

    @Signature
    private void __construct() { }
}
