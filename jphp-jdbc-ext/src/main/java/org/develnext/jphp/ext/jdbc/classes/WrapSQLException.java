package org.develnext.jphp.ext.jdbc.classes;

import org.develnext.jphp.ext.jdbc.JdbcExtension;
import php.runtime.annotation.Reflection.Name;
import php.runtime.env.Environment;
import php.runtime.ext.java.JavaException;
import php.runtime.reflection.ClassEntity;

@Name(JdbcExtension.NAMESPACE + "SQLException")
public class WrapSQLException extends JavaException {
    public WrapSQLException(Environment env, Throwable throwable) {
        super(env, throwable);
    }

    public WrapSQLException(Environment env, ClassEntity clazz) {
        super(env, clazz);
    }
}
