package org.develnext.jphp.ext.jdbc.classes;

import org.develnext.jphp.ext.jdbc.JdbcExtension;
import php.runtime.annotation.Reflection;
import php.runtime.annotation.Reflection.Name;
import php.runtime.annotation.Reflection.Signature;
import php.runtime.annotation.Reflection.WrapInterface;
import php.runtime.env.Environment;
import php.runtime.lang.BaseWrapper;
import php.runtime.reflection.ClassEntity;

import java.sql.DriverManager;

@Name(JdbcExtension.NAMESPACE + "DriverManager")
@WrapInterface(value = DriverManager.class, skipConflicts = true)
final public class WrapDriverManager extends BaseWrapper<DriverManager> {
    public WrapDriverManager(Environment env, DriverManager wrappedObject) {
        super(env, wrappedObject);
    }

    public WrapDriverManager(Environment env, ClassEntity clazz) {
        super(env, clazz);
    }

    @Signature
    private void __construct() { }

    @Signature
    public static boolean register(String driverClass) {
        try {
            Class.forName(driverClass);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }
}
