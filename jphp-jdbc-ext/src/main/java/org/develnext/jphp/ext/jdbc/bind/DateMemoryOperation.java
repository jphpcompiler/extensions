package org.develnext.jphp.ext.jdbc.bind;

import php.runtime.Memory;
import php.runtime.env.Environment;
import php.runtime.env.TraceInfo;
import php.runtime.ext.core.classes.time.WrapTime;
import php.runtime.memory.ObjectMemory;
import php.runtime.memory.support.MemoryOperation;
import php.runtime.reflection.ParameterEntity;

import java.sql.Date;

public class DateMemoryOperation extends MemoryOperation<Date> {
    @Override
    public Class<?>[] getOperationClasses() {
        return new Class<?>[] { Date.class };
    }

    @Override
    public Date convert(Environment environment, TraceInfo traceInfo, Memory memory) {
        return memory.isNull() ? null : new Date(memory.toObject(WrapTime.class).getDate().getTime());
    }

    @Override
    public Memory unconvert(Environment environment, TraceInfo traceInfo, Date date) {
        return new ObjectMemory(new WrapTime(environment, new java.util.Date(date.getTime())));
    }

    @Override
    public void applyTypeHinting(ParameterEntity parameter) {
        parameter.setTypeNativeClass(WrapTime.class);
    }
}
