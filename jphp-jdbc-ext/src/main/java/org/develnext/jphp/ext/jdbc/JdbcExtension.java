package org.develnext.jphp.ext.jdbc;

import org.develnext.jphp.ext.jdbc.bind.DateMemoryOperation;
import org.develnext.jphp.ext.jdbc.bind.TimeMemoryOperation;
import org.develnext.jphp.ext.jdbc.classes.WrapDriver;
import org.develnext.jphp.ext.jdbc.classes.WrapDriverManager;
import org.develnext.jphp.ext.jdbc.classes.WrapSQLException;
import php.runtime.annotation.Reflection;
import php.runtime.env.CompileScope;
import php.runtime.ext.support.Extension;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcExtension extends Extension {
    public final static String NAMESPACE = "ext\\jdbc\\";

    @Override
    public String getVersion() {
        return "1.0.0";
    }

    @Override
    public void onRegister(CompileScope scope) {
        registerJavaException(scope, WrapSQLException.class, SQLException.class);

        registerMemoryOperation(DateMemoryOperation.class);
        registerMemoryOperation(TimeMemoryOperation.class);

        registerWrapperClass(scope, Driver.class, WrapDriver.class);
        registerWrapperClass(scope, DriverManager.class, WrapDriverManager.class);
    }
}
