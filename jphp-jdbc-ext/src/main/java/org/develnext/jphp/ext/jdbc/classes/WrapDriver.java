package org.develnext.jphp.ext.jdbc.classes;

import org.develnext.jphp.ext.jdbc.JdbcExtension;
import php.runtime.annotation.Reflection;
import php.runtime.annotation.Reflection.Name;
import php.runtime.annotation.Reflection.Signature;
import php.runtime.annotation.Reflection.WrapInterface;
import php.runtime.env.Environment;
import php.runtime.lang.BaseWrapper;
import php.runtime.reflection.ClassEntity;

import java.sql.Driver;

@Name(JdbcExtension.NAMESPACE)
@WrapInterface(value = Driver.class, skipConflicts = true)
public class WrapDriver extends BaseWrapper<Driver> {
    public WrapDriver(Environment env, Driver wrappedObject) {
        super(env, wrappedObject);
    }

    public WrapDriver(Environment env, ClassEntity clazz) {
        super(env, clazz);
    }

    @Signature
    private void __construct() { }
}
