<?php

use ext\libgdx\Application;
use ext\libgdx\assets\AssetManager;
use ext\libgdx\LwjglApplication;
use ext\libgdx\LwjglApplicationConfiguration;
use ext\libgdx\ApplicationListener;
use ext\libgdx\Gdx;
use ext\libgdx\graphics\Sprite;
use ext\libgdx\graphics\SpriteBatch;
use ext\libgdx\graphics\Texture;
use ext\libgdx\math\Polygon;
use ext\libgdx\math\Polyline;
use ext\libgdx\math\Vector2;

class MyGame implements ApplicationListener {

    /**
     * @var SpriteBatch
     */
    protected $batch;

    /**
     * @var AssetManager
     */
    public $assets;

    function create()
    {
        Gdx::app()->setLogLevel(Application::LOG_DEBUG);

        $this->batch = new SpriteBatch();
        $this->assets = new AssetManager(function($fileName) {
            return Gdx::files()->classpath("assets/" . $fileName);
        });

        $this->assets->loadTexture('DevelNext-Logo.png');
        $this->assets->loadMusic('music.mp3');

        $this->assets->finishLoading();

        /** @var \ext\libgdx\audio\Music $music */
        $music = $this->assets->get('music.mp3');
        $music->play();
    }

    function resize($width, $height)
    {

    }

    function render()
    {
        $this->batch->begin();


            $sprite = new Sprite($this->assets->get('DevelNext-Logo.png'));
            $sprite->draw($this->batch);

        $this->batch->end();
    }

    function pause()
    {

    }

    function resume()
    {

    }

    function dispose()
    {

    }
}

$config = new LwjglApplicationConfiguration();
$config->title = 'My Super Game';

$app = new LwjglApplication(new MyGame(), $config);