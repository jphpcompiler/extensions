<?php

use ext\javafx\UXApplication;
use ext\javafx\UXButton;
use ext\javafx\UXLoader;
use ext\javafx\UXStage;
use php\io\Stream;

ob_implicit_flush(true);

UXApplication::setTheme(Stream::of('res://themes/Modena.css'));

UXApplication::runLater(function() {
    $loader = new UXLoader();

    $stage = $loader->loadAsStage(Stream::of('res://LoginForm.fxml'));

    /** @var UXButton $button */
    $button = $stage->loginBtn;

    $button->on('click', function () use ($stage) {
        $dialog = new UXStage();
        $dialog->owner = $stage;
        $dialog->modality = 'APPLICATION_MODAL';

        $dialog->show();
    });

    $stage->centerOnScreen();
    $stage->show();
});