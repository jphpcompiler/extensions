<?php
namespace ext\libgdx;

class LwjglApplication extends Application {
    /**
     * @param ApplicationListener $listener
     * @param LwjglApplicationConfiguration $configuration
     */
    public function __construct(ApplicationListener $listener, LwjglApplicationConfiguration $configuration) { }
}
