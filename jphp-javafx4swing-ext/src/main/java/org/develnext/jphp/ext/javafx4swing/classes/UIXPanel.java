package org.develnext.jphp.ext.javafx4swing.classes;

import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import org.develnext.jphp.ext.javafx4swing.JavaFX4SwingExtension;
import org.develnext.jphp.swing.classes.components.support.UIContainer;
import php.runtime.Memory;
import php.runtime.annotation.Reflection.Getter;
import php.runtime.annotation.Reflection.Name;
import php.runtime.annotation.Reflection.Setter;
import php.runtime.env.Environment;
import php.runtime.reflection.ClassEntity;

import java.awt.*;

@Name(JavaFX4SwingExtension.NAMESPACE + "UIXPanel")
public class UIXPanel extends UIContainer {
    protected JFXPanel jfxPanel;

    public UIXPanel(Environment env, JFXPanel jfxPanel) {
        super(env);
        this.jfxPanel = jfxPanel;
    }

    public UIXPanel(Environment env, ClassEntity clazz) {
        super(env, clazz);
    }

    @Override
    public void setComponent(Component component) {
        this.jfxPanel = (JFXPanel) component;
    }

    @Override
    protected void onInit(Environment environment, Memory... memories) {
        this.jfxPanel = new JFXPanel();
    }

    @Override
    public Container getContainer() {
        return jfxPanel;
    }

    @Getter
    protected Scene getScene() {
        return jfxPanel.getScene();
    }

    @Setter
    protected void setScene(Scene scene) {
        jfxPanel.setScene(scene);
    }
}
