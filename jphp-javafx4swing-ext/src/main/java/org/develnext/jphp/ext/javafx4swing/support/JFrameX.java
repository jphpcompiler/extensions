package org.develnext.jphp.ext.javafx4swing.support;

import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import org.develnext.jphp.swing.support.RootWindow;

import java.awt.*;

public class JFrameX extends org.develnext.jphp.swing.support.JFrameX implements RootWindow {
    protected final JFXPanel fxPanel;
    protected Scene scene;
    protected final AnchorPane layout;

    public JFrameX() {
        setContentPane(fxPanel = new JFXPanel());

        layout = new AnchorPane();

        scene = new Scene(layout);
        fxPanel.setScene(scene);
    }

    public void moveToCenter() {
        this.setLocationRelativeTo((Component)null);
    }

    public Scene getScene() {
        return scene;
    }

    public AnchorPane getPane() {
        return layout;
    }

    public void setBackground(Color bgColor) {
        if (fxPanel != null) fxPanel.setBackground(bgColor);
    }

    public Color getBackground() {
        if (fxPanel != null && fxPanel.isBackgroundSet()) {
            return fxPanel.getBackground();
        } else {
            return Color.WHITE;
        }
    }
}
