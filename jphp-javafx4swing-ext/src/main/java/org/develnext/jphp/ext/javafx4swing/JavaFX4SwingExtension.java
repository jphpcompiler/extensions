package org.develnext.jphp.ext.javafx4swing;

import javafx.embed.swing.JFXPanel;
import org.develnext.jphp.ext.javafx4swing.classes.UIXPanel;
import org.develnext.jphp.swing.SwingExtension;
import php.runtime.env.CompileScope;

public class JavaFX4SwingExtension extends SwingExtension {
    @Override
    public void onRegister(CompileScope scope) {
        registerClass(scope, UIXPanel.class, JFXPanel.class);
    }
}
