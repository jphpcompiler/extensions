<?php
namespace php\swing;

use ext\javafx\UXScene;

/**
 * Class UIXPanel
 * @package php\swing
 */
class UIXPanel extends UIContainer
{
    /**
     * @var UXScene
     */
    public $scene;
}